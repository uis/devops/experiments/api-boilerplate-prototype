# People-related policies for local development.

package authz
import future.keywords

allow if {
    user_is_on_allow_list
    url_name == "person-list"
    is_query
}

allow if {
    user_is_on_allow_list
    url_name == "person-detail"
    is_query
}
