package authz
import future.keywords

import data.authz.allow

mock_input := {
  "request": {
    "method": "GET",
    "resolver_match": {
      "url_name": "",
    },
    "auth": {
      "principal_identifier": "",
    }
  },
}

patch_principal_identifier(input_data, principal_identifier) := json.patch(input_data, [
    {"op": "replace", "path": "/request/auth/principal_identifier", "value": principal_identifier}
])

# A mock allow list
mock_allow_list := [
    "mock1@example.invalid",
    "mock2@example.invalid",
]

test_default_is_disallow if {
    not allow
}

# People-related tests.

people_get_input := json.patch(mock_input, [
    {"op": "replace", "path": "/request/resolver_match/url_name", "value": "person-list"}
])

test_default_get_people_allowed_if_on_allow_list if {
    every principal_identifier in mock_allow_list {
        allow with input as patch_principal_identifier(people_get_input, principal_identifier)
            with data.allow_list as mock_allow_list
    }
}

test_default_get_people_not_allowed_if_not_on_allow_list if {
    every principal_identifier in ["other@example.invalid"] {
        not allow with input as patch_principal_identifier(people_get_input, principal_identifier)
            with data.allow_list as mock_allow_list
    }
}
