# Authorisation policies for local development.
package authz
import future.keywords

default allow = false

# True if and only if the auth token has been granted one of the passed scopes.
has_any_scope_of(scopes) {
    input.request.auth.scopes[_] == scopes[_]
}

# True if the authenticated principal is on the allow list for the API.
user_is_on_allow_list := input.request.auth.principal_identifier in data.allow_list

# True if this is a query-only method.
is_query := input.request.method in ["GET", "HEAD", "OPTIONS"]

# Django url name
url_name := input.request.resolver_match.url_name

# Allow API root to all users on allow list.
allow if {
    user_is_on_allow_list
    url_name == "api-root"
}
