###############################################################################
# Base image for all others to build upon.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.11-slim AS base

# Some performance and disk-usage optimisations for Python within a docker container.
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

WORKDIR /usr/src/app

# Pretty much everything from here on needs poetry.
RUN pip install --no-cache-dir poetry

###############################################################################
# Just enough to run tox. Tox will install any other dependencies it needs.
FROM base AS tox

RUN pip install tox
ENTRYPOINT ["tox"]
CMD []

###############################################################################
# Install requirements.
FROM base AS installed-deps

COPY pyproject.toml poetry.lock ./
RUN set -e; \
  poetry export --format=requirements.txt --output=.tmp-requirements.txt; \
  pip install --no-cache-dir -r .tmp-requirements.txt; \
  rm .tmp-requirements.txt

# Default environment for image. By default, we use the settings module
# bundled with this repo. Change DJANGO_SETTINGS_MODULE to use custom settings.
ENV DJANGO_SETTINGS_MODULE=project.settings

###############################################################################
# A development-focussed image. This does not include the actual application
# code since that will be mounted in as a volume.
FROM installed-deps AS development

# Additionally, install local development requirements.
RUN set -e; \
  poetry export --only=dev --format=requirements.txt --output=.tmp-requirements.txt; \
  pip install --no-cache-dir -r .tmp-requirements.txt; \
  rm .tmp-requirements.txt

ENV DJANGO_SETTINGS_MODULE=project.settings.developer

EXPOSE 8000
ENTRYPOINT ["python3", "-Xfrozen_modules=off", "-m", "debugpy", "--listen", "0.0.0.0:5678"]
CMD ["./manage.py", "runserver", "0.0.0.0:8000"]

###############################################################################
# The last target in the file is the "default" one. In our case it is the
# production image.
#
# KEEP THIS AS THE FINAL TARGET OR ELSE DEPLOYMENTS WILL BREAK.
FROM installed-deps AS production

# The production target includes the application code.
COPY . .

# We start a server on a port provided to us via the PORT environment variable.
ENV PORT=8000
ENTRYPOINT ["sh", "-c"]
CMD ["exec gunicorn \
  --name boilerplate_webapp \
  --bind 0.0.0.0:$PORT \
  --worker-class uvicorn.workers.UvicornWorker \
  --workers 4 --threads 1 \
  --log-level=info \
  --log-file=- \
  --access-logfile=- \
  --capture-output \
  project.asgi" \
]
