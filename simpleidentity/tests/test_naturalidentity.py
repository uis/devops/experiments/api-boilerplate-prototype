import pytest
from django.db.models import Max, Min
from django.utils.timezone import make_aware, now

from simpleidentity import models


@pytest.fixture
def people_and_natural_identities(faker, fake_factory):
    """
    Creates a set of people. Some are guaranteed to have no natural identities. Some will have
    exactly one. Some will have more than one.

    """
    # How many people for each class.
    n_people_per_class = 20

    # Maximum number of identities for those with more than 1.
    max_identities = 5

    # Create people with no identities
    fake_factory.bulk_people(n_people_per_class)

    # Create people with one natural identity
    for person in fake_factory.bulk_people(n_people_per_class):
        fake_factory.natural_identity(person=person)

    # Create people with more than one natural identity.
    for person in fake_factory.bulk_people(n_people_per_class):
        fake_factory.bulk_natural_identities(faker.random_int(2, max_identities), people=[person])


def test_filter_effective_only_returns_one_result_per_person(people_and_natural_identities):
    "filter_effective() returns only one result per person"
    people_ids = models.NaturalIdentity.objects.filter_effective().values_list(
        "person_id", flat=True
    )
    assert len(people_ids) > 0
    assert len(people_ids) == len(set(people_ids)), "Duplicate people returned from query"


def test_filter_effective_returns_all_people(people_and_natural_identities):
    "filter_effective() returns one result for each person with an effective identity"
    people_ids = models.NaturalIdentity.objects.filter_effective().values_list(
        "person_id", flat=True
    )
    expected_people_ids = models.Person.objects.filter(
        natural_identities__isnull=False
    ).values_list("pk", flat=True)
    assert set(people_ids) == set(expected_people_ids)


def test_filter_effective_returns_current_identity(people_and_natural_identities):
    "filter_effective() returns only the most recent effective identities"
    for natural_identity in models.NaturalIdentity.objects.filter_effective():
        other_identities_for_person = (
            models.NaturalIdentity.objects.filter(person=natural_identity.person)
            .exclude(pk=natural_identity.pk)
            .all()
        )
        for other_identity in other_identities_for_person:
            assert other_identity.became_effective_at < natural_identity.became_effective_at


def test_filter_effective_returns_only_identities_in_the_past(
    people_and_natural_identities, faker, fake_factory
):
    "filter_effective() returns only identities from the past"
    person = models.Person.objects.order_by("?")[0]
    future_identity = fake_factory.natural_identity(person=person)
    then = now()
    future_identity.became_effective_at = make_aware(faker.future_datetime())
    future_identity.save()
    all_effective_identities = set(
        models.NaturalIdentity.objects.filter_effective(then).values_list("pk", flat=True)
    )
    assert future_identity.pk not in all_effective_identities


def test_filter_effective_custom_effective_at(people_and_natural_identities, faker):
    "filter_effective() can take a custom effective at time"
    # Choose a random effective at time based on the minimum and maximum effective times for our
    # identities.
    aggregates = models.NaturalIdentity.objects.aggregate(
        after=Min("became_effective_at"), before=Max("became_effective_at")
    )
    effective_at = make_aware(faker.date_time_between(aggregates["after"], aggregates["before"]))
    all_effective_identities_became_effective_at = set(
        models.NaturalIdentity.objects.filter_effective(effective_at).values_list(
            "became_effective_at", flat=True
        )
    )
    for became_effective_at in all_effective_identities_became_effective_at:
        assert became_effective_at <= effective_at
