import pytest
from django.core.management import CommandError, call_command

from simpleidentity import models


@pytest.mark.django_db
def test_insert_fake_data_command():
    "The insertfakeidentitydata command succeeds and inserts data."
    assert not models.Person.objects.exists()
    assert not models.NaturalIdentity.objects.exists()
    assert not models.Institution.objects.exists()
    assert not models.Group.objects.exists()
    call_command(
        "insertfakeidentitydata",
        "--people=200",
        "--groups=20",
        "--institutions=10",
        "--unidentified-people=5",
    )
    assert models.Person.objects.count() == 200
    assert models.Person.objects.filter(natural_identities__isnull=True).count() == 5
    assert models.Group.objects.count() == 20
    assert models.Institution.objects.count() == 10


@pytest.mark.django_db
def test_people_must_be_more_than_unidentified_people():
    "We cannot ask for more unidentified people than people."
    with pytest.raises(CommandError):
        call_command(
            "insertfakeidentitydata",
            "--people=10",
            "--unidentified-people=20",
        )
