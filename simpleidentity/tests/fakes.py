"""
Pytest utilities and fixtures for creating fake objects.

"""
import pytest

from simpleidentity.fakefactory import FakeFactory


@pytest.fixture
def fake_factory(db, faker):
    "An instance of FakeFactory which can create fake objects in the database."
    return FakeFactory(faker)
