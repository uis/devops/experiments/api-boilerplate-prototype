import pytest

from simpleidentity import models


@pytest.fixture
def fake_entities(fake_factory):
    return (
        [fake_factory.person() for _ in range(10)]
        + [fake_factory.institution() for _ in range(10)]
        + [fake_factory.group() for _ in range(10)]
    )


def test_creating_person_creates_entity(fake_factory):
    """
    Creating a person should create an associated entity.

    """
    person = fake_factory.person()
    assert person.entity is not None
    assert person.entity.person is not None
    assert person.entity.person.pk == person.pk


def test_creating_group_creates_entity(fake_factory):
    """
    Creating a group should create an associated entity.

    """
    group = fake_factory.group()
    assert group.entity is not None
    assert group.entity.group is not None
    assert group.entity.group.pk == group.pk


def test_creating_institution_creates_entity(fake_factory):
    """
    Creating a institution should create an associated entity.

    """
    institution = fake_factory.institution()
    assert institution.entity is not None
    assert institution.entity.institution is not None
    assert institution.entity.institution.pk == institution.pk


def test_querying_only_people(fake_entities):
    """
    Querying for Person entities only returns those with a related Person.

    """
    expected_entity_ids = {o.entity_id for o in models.Person.objects.all()}
    actual_entity_ids = {o.pk for o in models.Entity.objects.filter(person__isnull=False).all()}
    assert actual_entity_ids == expected_entity_ids


def test_querying_only_groups(fake_entities):
    """
    Querying for Group entities only returns those with a related Group.

    """
    expected_entity_ids = {o.entity_id for o in models.Group.objects.all()}
    actual_entity_ids = {o.pk for o in models.Entity.objects.filter(group__isnull=False).all()}
    assert actual_entity_ids == expected_entity_ids


def test_querying_only_institutions(fake_entities):
    """
    Querying for Institution entities only returns those with a related Institution.

    """
    expected_entity_ids = {o.entity_id for o in models.Institution.objects.all()}
    actual_entity_ids = {
        o.pk for o in models.Entity.objects.filter(institution__isnull=False).all()
    }
    assert actual_entity_ids == expected_entity_ids
