from django.apps import AppConfig


class SimpleIdentityConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "simpleidentity"
    verbose_name = "Simple Identity Data Model"
