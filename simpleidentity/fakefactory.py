"""
Utilities and fixtures for creating fake objects.

"""
from django.utils.timezone import make_aware
from faker import Faker

from . import models


class FakeFactory:
    """
    A factory class which can create fake objects. Initialised with a faker.Faker instance.

    """

    def __init__(self, faker=None):
        self.faker = faker or Faker()

    def _bulk_entities(self, count):
        """Bulk create entities."""
        return models.Entity.objects.bulk_create([models.Entity() for _ in range(count)])

    def bulk_people(self, count):
        """Bulk insert person objects."""
        return models.Person.objects.bulk_create(
            [models.Person(entity=entity) for entity in self._bulk_entities(count)]
        )

    def person(self):
        """A fake Person object. The object exists in the database."""
        return self.bulk_people(1)[0]

    def bulk_groups(self, count):
        """Bulk insert group objects."""
        return models.Group.objects.bulk_create(
            [models.Group(entity=entity) for entity in self._bulk_entities(count)]
        )

    def group(self):
        """A fake Group object. The object exists in the database."""
        return self.bulk_groups(1)[0]

    def bulk_institutions(self, count):
        """Bulk insert institution objects."""
        return models.Institution.objects.bulk_create(
            [models.Institution(entity=entity) for entity in self._bulk_entities(count)]
        )

    def institution(self):
        """A fake institution object. The object exists in the database."""
        return self.bulk_institutions(1)[0]

    def bulk_natural_identities(self, count, *, people=None):
        """
        Natural identities for a people randomly selected from "people". If no list of people is
        provided, the entire set of people will be fetched from the database.

        """
        people = people or models.Person.objects.all()
        return models.NaturalIdentity.objects.bulk_create(
            [
                models.NaturalIdentity(
                    person=self.faker.random_element(people),
                    first_name=self.faker.first_name() if self.faker.boolean(95) else "",
                    middle_name=" ".join(
                        self.faker.first_name() for _ in range(self.faker.random_int(1, 3))
                    )
                    if self.faker.boolean(75)
                    else "",
                    last_name=self.faker.last_name(),
                    name_suffix=" ".join(
                        self.faker.suffix() for _ in range(self.faker.random_int(1, 2))
                    )
                    if self.faker.boolean(10)
                    else "",
                    became_effective_at=make_aware(self.faker.date_time_this_decade()),
                )
                for _ in range(count)
            ]
        )

    def natural_identity(self, *, person=None):
        """
        A fake natural identity for the passed person. If no person is passed, one is selected
        randomly from the database.

        """
        person = person or models.Person.objects.order_by("?")[0]
        return self.bulk_natural_identities(1, people=[person])[0]
