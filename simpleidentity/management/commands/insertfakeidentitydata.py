import random

from django.core.management.base import BaseCommand, CommandError

from simpleidentity import models
from simpleidentity.fakefactory import FakeFactory


class Command(BaseCommand):
    help = "Inserts a set of fake identity data"

    def add_arguments(self, parser):
        parser.add_argument("--people", type=int, default=2000)
        parser.add_argument("--groups", type=int, default=200)
        parser.add_argument("--institutions", type=int, default=20)
        parser.add_argument("--unidentified-people", type=int, default=10)

    def handle(self, *args, **options):
        factory = FakeFactory()

        # Validate command-line parameters.
        if options["unidentified_people"] > options["people"]:
            raise CommandError("Cannot have more unidentified people than people.")

        # Create top-level entities.
        self.stdout.write(f"Inserting {options['people']} people... ", ending="")
        people = factory.bulk_people(options["people"])
        self.stdout.write(self.style.SUCCESS("done"))

        self.stdout.write(f"Inserting {options['groups']} groups... ", ending="")
        factory.bulk_groups(options["groups"])
        self.stdout.write(self.style.SUCCESS("done"))

        self.stdout.write(f"Inserting {options['institutions']} institutions... ", ending="")
        factory.bulk_institutions(options["institutions"])
        self.stdout.write(self.style.SUCCESS("done"))

        # Choose a small number of people who will never get identities.
        random.shuffle(people)
        unidentified_people_keys = {
            person.pk for person in people[: options["unidentified_people"]]
        }

        # Choose a set of people who should never get identities.
        unidentified_people_qs = models.Person.objects.filter(
            pk__in={person.pk for person in people}, natural_identities__isnull=True
        ).exclude(pk__in=unidentified_people_keys)

        # Create natural identities for people until everyone has one.
        self.stdout.write("Inserting natural identities for people... ", ending="")
        while unidentified_people_qs.exists():
            people_with_no_identities = unidentified_people_qs.all()
            factory.bulk_natural_identities(
                len(people_with_no_identities), people=people_with_no_identities
            )
        self.stdout.write(self.style.SUCCESS("done"))
