from django.contrib import admin
from django.db.models import OuterRef, Prefetch, Subquery
from django.utils.timezone import now

from . import models


class NaturalIdentityInline(admin.StackedInline):
    model = models.NaturalIdentity
    ordering = ("-became_effective_at",)
    readonly_fields = ("became_effective_at", "updated_at")
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("last_name", "unique_id", "display_name")
    list_display_links = ("last_name", "unique_id")
    inlines = [NaturalIdentityInline]
    search_fields = ("last_name",)
    ordering = ("pk",)
    readonly_fields = ("created_at",)

    class Media:
        css = {"all": ["simpleidentity/admin.css"]}

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        effective_at = now()
        return (
            qs.prefetch_related(
                Prefetch(
                    "natural_identities",
                    queryset=models.NaturalIdentity.objects.filter_effective(effective_at),
                    to_attr="effective_natural_identities",
                )
            )
            .annotate(
                last_name=Subquery(
                    models.NaturalIdentity.objects.filter(
                        became_effective_at__lte=effective_at, person_id=OuterRef("pk")
                    )
                    .distinct("person_id")
                    .order_by("person_id", "-became_effective_at")
                    .values("last_name")[:1]
                )
            )
            .order_by("pk")
        )

    @admin.display(ordering="pk")
    def unique_id(self, obj):
        return obj.pk

    def last_name(self, obj):
        return obj.last_name

    def display_name(self, obj):
        try:
            return obj.effective_natural_identities[0].get_display_name()
        except IndexError:
            return None

    def has_delete_permission(self, request, obj=None):
        return False
