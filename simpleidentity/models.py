"""
Models used by the API.

"""
import secrets
import uuid

from django.db import models
from django.utils.timezone import now


def _make_uuid4():
    return uuid.UUID(bytes=secrets.token_bytes(16), version=4)


class UUIDPrimaryKeyMixin(models.Model):
    """
    Add an "id" primary key field to the model which is an automatically generated UUID.

    The UUID is guaranteed to have been generated using a cryptographically safe random number
    source.
    """

    id = models.UUIDField(
        default=_make_uuid4,
        primary_key=True,
        unique=True,
        editable=False,
    )

    class Meta:
        abstract = True


class TimestampFieldsMixin(models.Model):
    """
    Add automatically managed "created_at" and "updated_at" fields to the model.

    """

    created_at = models.DateTimeField(
        blank=False,
        auto_now_add=True,
        editable=False,
        help_text="Date and time at which this resource record was first created",
    )

    updated_at = models.DateTimeField(
        blank=False,
        auto_now=True,
        editable=False,
        help_text="Date and time at which this resource record was last changed",
    )

    class Meta:
        abstract = True


class Entity(UUIDPrimaryKeyMixin, TimestampFieldsMixin):  # type: ignore[misc]
    """
    The abstract concept of an "entity". Top-level entities live in the same namespace by means of
    using a foreign key to an Entity as their primary key.
    """


class Person(TimestampFieldsMixin):
    """
    A natural person. A Person is an Entity. Its primary key is a foreign key pointing to a parent
    entity.
    """

    entity = models.OneToOneField(
        Entity, primary_key=True, editable=False, on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = "people"


class Group(TimestampFieldsMixin):
    """
    A group of people. A Group is an Entity. Its primary key is a foreign key pointing to a parent
    entity.
    """

    entity = models.OneToOneField(
        Entity, primary_key=True, editable=False, on_delete=models.CASCADE
    )


class Institution(TimestampFieldsMixin):
    """
    An institution within the University. A institution is an Entity. Its primary key is a foreign
    key pointing to a parent entity.
    """

    entity = models.OneToOneField(
        Entity, primary_key=True, editable=False, on_delete=models.CASCADE
    )


class NaturalIdentityQuerySet(models.QuerySet):
    """
    Custom NaturalIdentity queryset with utility methods.

    """

    def filter_effective(self, effective_at=None):
        """
        Filter the query to only those natural identities which are currently effective.

        By default the filtering will be for those effective "now" but this can be changed by
        providing a datetime instance in the "effective_at" argument.

        IMPORTANT: this adds an implicit order_by() to the queryset. You cannot change the
        order_by() afterwards without breaking this filtering.

        """
        effective_at = effective_at or now()
        return (
            self.filter(became_effective_at__lte=effective_at)
            .distinct("person_id")
            .order_by("person_id", "-became_effective_at")
        )


NaturalIdentityManager = models.Manager.from_queryset(NaturalIdentityQuerySet)


class NaturalIdentity(UUIDPrimaryKeyMixin, TimestampFieldsMixin):
    """
    An identity for a natural person.
    """

    # The natural primary key for a natural identity would be (person, became_effective_at) but
    # Django does not support composite primary keys. As such we have to make do with a uniqueness
    # constraint.

    person = models.ForeignKey(
        Person,
        null=False,
        on_delete=models.CASCADE,
        related_name="natural_identities",
        editable=False,
    )
    became_effective_at = models.DateTimeField(default=now, editable=False)

    # Only the "last_name" is required. The rest may be blank.
    first_name = models.CharField(blank=True, default="")
    middle_name = models.CharField(blank=True, default="")
    last_name = models.CharField()
    name_suffix = models.CharField(blank=True, default="")

    objects = NaturalIdentityManager()

    class Meta:
        verbose_name_plural = "natural identities"
        indexes = [
            models.Index(
                fields=["person_id", "became_effective_at"], name="person_became_effective_at"
            ),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["person_id", "became_effective_at"],
                name="natural_identity_composite_pk_constaint",
            ),
        ]

    def get_display_name(self):
        components = [self.first_name, self.middle_name, self.last_name, self.name_suffix]
        components = [c for c in components if c != ""]
        return " ".join(components)

    def __str__(self):
        return self.get_display_name()
