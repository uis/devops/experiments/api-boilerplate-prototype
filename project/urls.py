"""
Boilerplate Webapp URL Configuration

"""
import structlog
from django.conf import settings
from django.contrib import admin
from django.dispatch import receiver
from django.http import HttpResponse
from django.urls import include, path
from django_structlog import signals


# There is no real reason to configure logging like this here except that it's a convenient place
# which runs at application start.
@receiver(signals.bind_extra_request_metadata)
@receiver(signals.bind_extra_request_failed_metadata)
@receiver(signals.bind_extra_request_finished_metadata)
def bind_auth_info(request, logger, **kwargs):
    user_id = request.META.get("HTTP_X_API_OAUTH2_USER")
    structlog.contextvars.bind_contextvars(user_id=user_id)


urlpatterns = [
    # use `healthy` rather than `healthz` as Cloud Run reserves the use of the `/healthz` endpoint
    path(
        "healthy",
        lambda request: HttpResponse('{"status":"ok"}', content_type="application/json"),
        name="healthy",
    ),
    path("", include("api.urls")),
]

# Add in the django admin URLs if we're enabling the admin at a prefix unlikely to collide with any
# API routes.
if settings.ENABLE_ADMIN:
    urlpatterns[0:0] = [path("-/admin/", admin.site.urls)]

# Enable the Django debug toolbar if we are in debug mode.
if settings.DEBUG:
    import debug_toolbar

    urlpatterns[0:0] = [path(r"__debug__/", include(debug_toolbar.urls))]
