"""
Test that the registered system checks work as expected.

"""
from django.core.management import call_command


def test_checks_pass():
    """The system checks should succeed in the test suite configuration."""
    call_command("check")
