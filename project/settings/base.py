import os
import sys

import externalsettings
import structlog

# By default, make use of connection pooling for the default database and use the Postgres engine.
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "CONN_MAX_AGE": 60,  # seconds
    },
}

# In production we do not enable the admin unless overridden externally.
ENABLE_ADMIN = False

# If the EXTRA_SETTINGS_URLS environment variable is set, it is a comma-separated list of URLs from
# which to fetch additional settings as YAML-formatted documents. The documents should be
# dictionaries and top-level keys are imported into this module's global values.
_external_setting_urls = []
_external_setting_urls_list = os.environ.get("EXTRA_SETTINGS_URLS", "").strip()
if _external_setting_urls_list != "":
    _external_setting_urls.extend(_external_setting_urls_list.split(","))

try:
    externalsettings.load_external_settings(
        globals(),
        urls=_external_setting_urls,
        required_settings=[
            "SECRET_KEY",
            "DATABASES",
            "OPA_API_URL",
        ],
        optional_settings=[
            "EMAIL_HOST",
            "EMAIL_HOST_PASSWORD",
            "EMAIL_HOST_USER",
            "EMAIL_PORT",
            "ENABLE_ADMIN",
        ],
    )
except externalsettings.InvalidExternalSettingsError as e:
    print(f"Error loading external settings: {e}", file=sys.stderr)
    sys.exit(1)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# In production we do not enable the API auto-generated documentation ui
ENABLE_API_DOCS_UI = False

# By default, all hosts are allowed.
ALLOWED_HOSTS = ["*"]

# Installed applications
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "crispy_forms",
    "django_filters",
    "drf_spectacular",
    "rest_framework",
    "rest_framework.authtoken",
    "simpleidentity",
    "api",
]

# Installed middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django_structlog.middlewares.RequestMiddleware",
]

# Root URL patterns
ROOT_URLCONF = "project.urls"

# Template loading
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# Password validation
#
# .. seealso:: https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
#
# .. seealso:: https://docs.djangoproject.com/en/2.0/topics/i18n/
LANGUAGE_CODE = "en-gb"

# Internationalization
TIME_ZONE = "UTC"

# Internationalization
USE_I18N = True

# Internationalization
USE_TZ = True

# Static files (CSS, JavaScript, Images). We use a prefix which is unlikely to collide with any
# hosted API routes. This is unused in production but in development it may be used to serve the
# admin.
STATIC_URL = "-/static-something-else/"

# Authentication backends
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

# Configure DRF to use Django's session authentication to determine the current user
REST_FRAMEWORK = {
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "api.authentication.APIGatewayAuthentication",
    ],
    "DEFAULT_RENDERER_CLASSES": [
        "djangorestframework_camel_case.render.CamelCaseJSONRenderer",
    ],
    "DEFAULT_PARSER_CLASSES": [
        "djangorestframework_camel_case.parser.CamelCaseJSONParser",
        "djangorestframework_camel_case.parser.CamelCaseFormParser",
        "djangorestframework_camel_case.parser.CamelCaseMultiPartParser",
    ],
    "DEFAULT_PERMISSION_CLASSES": [
        "api.authorization.HasRequiredScope",
        "api.authorization.AllowedByOPAPolicy",
    ],
}

# settings for the OpenAPI schema generator
# https://drf-spectacular.readthedocs.io/en/latest/settings.html?highlight=settings
SPECTACULAR_SETTINGS = {
    # Custom configuration for our particular API.
    "TITLE": "Boilerplate API",
    "DESCRIPTION": "Boilerplate API",
    "VERSION": "1.0.0",
    "CONTACT": {
        "name": "UIS DevOps Division",
        "url": "https://guidebook.devops.uis.cam.ac.uk/",
        "email": "devops@uis.cam.ac.uk",
    },
    "OAUTH2_SCOPES": {
        "boilerplate": "Read/write access to full API",
        "boilerplate:readonly": "Read-only access to full API",
        "boilerplate/entities": "Read/write access to entities",
        "boilerplate/entities:readonly": "Read-only access to entities",
        "boilerplate/people": "Read/write access to people",
        "boilerplate/people:readonly": "Read-only access to people",
    },
    # Usual configuration for DRF APIs in general.
    "SERVE_INCLUDE_SCHEMA": False,
    "CAMELIZE_NAMES": True,
    "POSTPROCESSING_HOOKS": [
        "drf_spectacular.hooks.postprocess_schema_enums",
        "drf_spectacular.contrib.djangorestframework_camel_case.camelize_serializer_fields",
    ],
    "OAUTH2_FLOWS": ["clientCredentials", "authorizationCode"],
    "OAUTH2_AUTHORIZATION_URL": "https://api.apps.cam.ac.uk/oauth2/v1/auth",
    "OAUTH2_TOKEN_URL": "https://api.apps.cam.ac.uk/oauth2/v1/token",
}

# Allow all origins to access API.
CORS_URLS_REGEX = r"^.*$"
CORS_ORIGIN_ALLOW_ALL = True

# We also support the X-Forwarded-Proto header to detect if we're behind a load balancer which does
# TLS termination for us. In future this setting might need to be moved to settings.docker or to be
# configured via an environment variable if we want to support a wider range of TLS terminating
# load balancers.
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# Whether to support the x_forwarded_host in constructing the canonical url.
USE_X_FORWARDED_HOST = True

_structlog_foreign_pre_chain = [
    structlog.stdlib.add_log_level,
    structlog.stdlib.add_logger_name,
    structlog.processors.TimeStamper(fmt="iso"),
]


LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
    },
    "formatters": {
        # This formatter logs as structured JSON suitable for use in Cloud hosting environments.
        "json_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.JSONRenderer(),
            "foreign_pre_chain": _structlog_foreign_pre_chain,
        },
        # This formatter logs as coloured text suitable for use by humans.
        "console_formatter": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.dev.ConsoleRenderer(colors=True),
            "foreign_pre_chain": _structlog_foreign_pre_chain,
        },
    },
    "handlers": {
        # If debug is enabled, we render using the pretty console formatter. If it is disabled we
        # render using the JSON formatter.
        "console": {
            "class": "logging.StreamHandler",
            "filters": ["require_debug_true"],
            "formatter": "console_formatter",
        },
        "json": {
            "class": "logging.StreamHandler",
            "filters": ["require_debug_false"],
            "formatter": "json_formatter",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console", "json"],
            "propagate": True,
            "level": "INFO",
        },
        # Enabling the "DEBUG" level in "django.request" allows us to see when we're using
        # middleware which prevents us using a fully-asynchronous stack.
        #
        # See: https://docs.djangoproject.com/en/4.2/topics/async/#async-views
        "django.request": {
            "handlers": ["console", "json"],
            "propagate": True,
            "level": "DEBUG",
        },
    },
}

structlog.configure(
    processors=[
        structlog.contextvars.merge_contextvars,
        structlog.stdlib.filter_by_level,
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=True,
)
