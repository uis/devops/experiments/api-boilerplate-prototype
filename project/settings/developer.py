# Import settings from the base settings file
from .base import *  # noqa: F401, F403

DEBUG = True
ENABLE_ADMIN = True
ENABLE_API_DOCS_UI = True

# WSGI/ASCI applications. Used only by "./manage.py runserver".
WSGI_APPLICATION = "project.wsgi.application"
ASGI_APPLICATION = "project.asgi.application"

# Make logging pretty.
LOGGING["handlers"]["console"]["formatter"] = "console_formatter"  # type: ignore # noqa: F405

MIDDLEWARE = MIDDLEWARE + [  # noqa: F405
    "django_cprofile_middleware.middleware.ProfilerMiddleware"
]

# Configure the django debug toolbar.
INSTALLED_APPS = (
    [
        "daphne",  # support ASGI with runserver
    ]
    + INSTALLED_APPS  # noqa: F405
    + [
        "debug_toolbar",
    ]
)

MIDDLEWARE = MIDDLEWARE + [  # noqa: F405
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

DEBUG_TOOLBAR_CONFIG = {
    # Bypass the local IP check since, within a docker container, we don't know what the host IP
    # address is likely to be.
    "SHOW_TOOLBAR_CALLBACK": lambda *args: True,
}

# Enable the browsable API renderer in development
REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"].extend(  # type: ignore # noqa: F405
    [
        "rest_framework.renderers.BrowsableAPIRenderer",
    ]
)

DJANGO_CPROFILE_MIDDLEWARE_REQUIRE_STAFF = False
