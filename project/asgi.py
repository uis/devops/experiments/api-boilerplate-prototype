import os

from django.core.asgi import get_asgi_application
from starlette.datastructures import Headers

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")


def respect_x_forwarded_prefix(app):
    async def middleware_app(scope, receive, send):
        headers = Headers(scope=scope)
        prefix = headers.get("x-forwarded-prefix")
        if prefix is not None:
            scope["root_path"] = prefix
        await app(scope, receive, send)

    return middleware_app


application = respect_x_forwarded_prefix(get_asgi_application())
