import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")


def respect_x_forwarded_prefix(app):
    def middleware_app(environ, start_response):
        prefix = environ.get("HTTP_X_FORWARDED_PREFIX")
        if prefix is not None:
            environ["SCRIPT_NAME"] = prefix
        return app(environ, start_response)

    return middleware_app


application = respect_x_forwarded_prefix(get_wsgi_application())
