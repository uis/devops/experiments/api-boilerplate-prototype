"""
URL routing schema for API

"""
from django.conf import settings
from django.urls import include, path
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularJSONAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register("entities", views.EntityViewSet)
router.register("people", views.PersonViewSet)

urlpatterns = [
    path("", include(router.urls)),
    # OpenAPI v3 schema
    path("-/schema.yaml", SpectacularAPIView.as_view(), name="schema-yaml"),
    path("-/schema.json", SpectacularJSONAPIView.as_view(), name="schema-json"),
]

if settings.ENABLE_API_DOCS_UI:
    # Expose the schema UI id enabled in the settings.
    urlpatterns.extend(
        [
            path(
                "-/swagger-ui/",
                SpectacularSwaggerView.as_view(url_name="schema-yaml"),
                name="swagger-ui",
            ),
            path(
                "-/redoc/",
                SpectacularRedocView.as_view(url_name="schema-yaml"),
                name="redoc",
            ),
        ]
    )
