"""
Views implementing the API endpoints.

"""
from django.db.models import Prefetch
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import pagination, viewsets

from simpleidentity import models

from . import serializers
from .authorization import required_scopes_viewset


class Pagination(pagination.CursorPagination):
    page_size = 100
    page_size_query_param = "page_size"
    max_page_size = 5000
    ordering = "created_at"

    # Forward port of https://github.com/encode/django-rest-framework/pull/8687 which should land
    # in DRF >3.14.0.
    def get_paginated_response_schema(self, schema):
        return {
            "type": "object",
            "properties": {
                "next": {
                    "type": "string",
                    "nullable": True,
                    "format": "uri",
                    "example": "http://example.invalid/?{cursor_query_param}=xyz".format(
                        cursor_query_param=self.cursor_query_param
                    ),
                },
                "previous": {
                    "type": "string",
                    "nullable": True,
                    "format": "uri",
                    "example": "http://example.invalid/?{cursor_query_param}=xvz".format(
                        cursor_query_param=self.cursor_query_param
                    ),
                },
                "results": schema,
            },
        }


class StandardOptionsMixin:
    pagination_class = Pagination
    ordering = ("created_at",)


@extend_schema_view(
    list=extend_schema(
        summary="List entities",
        description="Retrieve a paginated list of all entities.",
    ),
    retrieve=extend_schema(
        summary="Retrieve an entity",
        description="Retrieves a single entity.",
    ),
)
@required_scopes_viewset(
    [
        "boilerplate",
        "boilerplate:readonly",
        "boilerplate/entities",
        "boilerplate/entities:readonly",
    ]
)
class EntityViewSet(StandardOptionsMixin, viewsets.ReadOnlyModelViewSet):
    queryset = models.Entity.objects.all()
    serializer_class = serializers.EntitySerializer


@extend_schema_view(
    list=extend_schema(
        summary="List people",
        description="Retrieve a paginated list of all people.",
    ),
    retrieve=extend_schema(
        summary="Retrieve a person",
        description="Retrieves a single person.",
    ),
)
@required_scopes_viewset(
    [
        "boilerplate",
        "boilerplate:readonly",
        "boilerplate/people",
        "boilerplate/people:readonly",
    ]
)
class PersonViewSet(StandardOptionsMixin, viewsets.ReadOnlyModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer

    def get_queryset(self):
        return models.Person.objects.all().prefetch_related(
            Prefetch(
                "natural_identities",
                queryset=models.NaturalIdentity.objects.filter_effective(),
                to_attr="effective_natural_identities",
            )
        )
