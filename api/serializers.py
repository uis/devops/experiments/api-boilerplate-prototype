"""
Serializers for API models.

"""
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers

from simpleidentity import models


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            "Entity",
            summary="An entity",
            description="An example of an entity returned from the entities endpoint.",
            value={
                "url": "http://example.invalid/entities/9416a892-c77e-4c0e-88bc-cacbbb64453d",
                "id": "9416a892-c77e-4c0e-88bc-cacbbb64453d",
            },
            response_only=True,
        )
    ]
)
class EntitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Entity
        fields = ("url", "id")
        read_only_fields = ("id",)


class NaturalIdentitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.NaturalIdentity
        fields = (
            "first_name",
            "middle_name",
            "last_name",
            "name_suffix",
            "became_effective_at",
            "created_at",
        )
        read_only_fields = ("became_effective_at", "created_at")


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            "Person",
            summary="A person",
            description="An example of a person returned from the people endpoint.",
            value={
                "url": "http://localhost:8000/people/266c7392-b4dd-4038-91c9-1c9deb8c96f2/",
                "entityId": "266c7392-b4dd-4038-91c9-1c9deb8c96f2",
                "createdAt": "2023-07-01T08:06:56.290424Z",
                "effectiveNaturalIdentities": [
                    {
                        "firstName": "Lisa",
                        "middleName": "",
                        "lastName": "Finley",
                        "nameSuffix": "",
                        "becameEffectiveAt": "2021-07-21T04:21:58Z",
                        "createdAt": "2023-07-01T08:06:58.014605Z",
                    }
                ],
            },
            response_only=True,
        ),
    ]
)
class PersonSerializer(serializers.HyperlinkedModelSerializer):
    effective_natural_identities = NaturalIdentitySerializer(read_only=True, many=True)

    class Meta:
        model = models.Person
        fields = ("url", "entity_id", "created_at", "effective_natural_identities")
        read_only_fields = ("entity_id", "created_at")
