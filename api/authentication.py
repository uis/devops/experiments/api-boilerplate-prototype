# Note: lifted from API Gateway auth library for the moment.
#
# SEE: https://gitlab.developers.cam.ac.uk/uis/devops/django/api-gateway-auth/
from dataclasses import dataclass
from typing import Optional, Set

import structlog
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from drf_spectacular.extensions import OpenApiAuthenticationExtension
from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.request import Request

LOG = structlog.get_logger()


@dataclass(eq=True)
class APIGatewayAuthenticationDetails:
    """
    A dataclass representing the authentication information passed from the API Gateway.
    """

    principal_identifier: str
    scopes: Set[str]
    app_id: Optional[str] = None
    client_id: Optional[str] = None


class APIGatewayUser(AnonymousUser):
    def __init__(self, auth: APIGatewayAuthenticationDetails):
        super().__init__()
        self.username = self.id = self.pk = str(auth.principal_identifier)

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    def __str__(self):
        return self.username


class APIGatewayAuthentication(authentication.BaseAuthentication):
    """
    An Authentication provider which interprets the headers provided by the API Gateway.

    This library expects to only be used within an application that is deployed behind and can
    only be invoked by the API Gateway, and therefore relies on the fact that the headers
    provided are authoritative.

    """

    def authenticate(self, request: Request):
        if not request.META.get("HTTP_X_API_ORG_NAME", None):
            # bail early if we look like we're not being called by the API Gateway
            raise AuthenticationFailed("Request not from API Gateway")
        if not request.META.get("HTTP_X_API_OAUTH2_USER", None):
            raise AuthenticationFailed("Could not authenticate using x-api-* headers")

        scope_header = request.META.get("HTTP_X_API_OAUTH2_SCOPE", "")
        auth = APIGatewayAuthenticationDetails(
            principal_identifier=request.META["HTTP_X_API_OAUTH2_USER"],
            scopes=set(scope_header.split(" ")) if scope_header.strip() != "" else set(),
            # the following will only be populated for confidential clients
            app_id=request.META.get("HTTP_X_API_DEVELOPER_APP_ID", None),
            client_id=request.META.get("HTTP_X_API_OAUTH2_CLIENT_ID", None),
        )
        # the first item in the tuple represents the 'user' which we don't have when we've
        # used the API Gateway for authentication.
        user = APIGatewayUser(auth)
        LOG.info(
            "Authenticated request",
            principal_identifier=auth.principal_identifier,
            scopes=auth.scopes,
            app_id=auth.app_id,
            client_id=auth.client_id,
        )
        return user, auth


@dataclass
class SecurityRequirement:
    operations: set[str]
    scopes: list[str]


class APIGatewayAuthenticationScheme(OpenApiAuthenticationExtension):
    target_class = "api.authentication.APIGatewayAuthentication"
    name = "API Gateway"

    token_url = "https://api.apps.cam.ac.uk/oauth2/v1/token"
    authorization_url = "https://api.apps.cam.ac.uk/oauth2/v1/auth"
    flows = ["clientCredentials", "authorizationCode"]

    def get_security_definition(self, auto_schema):
        return {
            "type": "oauth2",
            "flows": {flow_name: self._make_flow(flow_name) for flow_name in self.flows},
        }

    def get_security_requirement(self, auto_schema):
        try:
            required_scopes_dict = auto_schema.view.required_scopes
        except AttributeError:
            LOG.warning("View does not define any required scopes.", view=auto_schema.view)
            return False
        action = getattr(auto_schema.view, "action", None)
        action_scopes = required_scopes_dict["per_action"].get(
            action, required_scopes_dict["default"]
        )
        return {self.name: action_scopes}

    def _make_flow(self, flow_name):
        spec_settings = getattr(settings, "SPECTACULAR_SETTINGS", {})
        flow = {
            "tokenUrl": self.token_url,
            "scopes": spec_settings.get("OAUTH2_SCOPES", {}),
        }

        if flow_name in ["implicit", "authorizationCode"]:
            flow["authorizationUrl"] = self.authorization_url

        return flow
