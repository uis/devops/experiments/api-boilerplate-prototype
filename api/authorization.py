import json
import urllib.parse

import requests
import structlog
from django.conf import settings
from django.utils.module_loading import import_string
from rest_framework import serializers
from rest_framework.permissions import BasePermission
from rest_framework.renderers import JSONRenderer

LOG = structlog.get_logger()


def required_scopes_viewset(default=None, **action_scopes):
    def wrapper(cls):
        cls.required_scopes = {
            "default": default if default is not None else [],
            "per_action": action_scopes,
        }
        return cls

    return wrapper


class HasRequiredScope(BasePermission):
    """
    Checks that the incoming request has at least one of the required scopes for the action.
    """

    message = "Your authentication token is missing a required scope."

    def has_permission(self, request, view):
        try:
            required_scopes_dict = view.required_scopes
        except AttributeError:
            LOG.warning("View does not define any required scopes.", view=view)
            return False

        action = getattr(view, "action", request.method.lower())
        action_scopes = required_scopes_dict["per_action"].get(
            action, required_scopes_dict["default"]
        )
        if len(action_scopes) == 0:
            LOG.warning("Required scope list is empty for action.", view=view, action=action)
            return False

        overlapping_scopes = set(request.auth.scopes) & set(action_scopes)
        has_at_least_one_scope = len(overlapping_scopes) > 0
        if not has_at_least_one_scope:
            LOG.info(
                "Denying request due to insufficient token scope",
                token_scopes=list(request.auth.scopes),
                required_scopes=list(action_scopes),
            )
        return has_at_least_one_scope


class APIGatewayAuthenticationDetailsSerializer(serializers.Serializer):
    principal_identifier = serializers.CharField()
    scopes = serializers.ListField(child=serializers.CharField())
    app_id = serializers.CharField(required=False)
    client_id = serializers.CharField(required=False)


class ResolverMatchSerializer(serializers.Serializer):
    url_name = serializers.CharField()
    route = serializers.CharField()
    app_name = serializers.CharField()
    app_names = serializers.ListField(child=serializers.CharField())
    namespace = serializers.CharField()
    namespaces = serializers.ListField(child=serializers.CharField())
    view_name = serializers.CharField(required=True)
    args = serializers.ListField(child=serializers.JSONField())
    kwargs = serializers.DictField(child=serializers.JSONField())


# Sensitive headers which should be redacted from the information sent to OPA. Note that these
# names must be in lower case.
SENSITIVE_HEADERS = {"authorization"}


class RequestSerializer(serializers.Serializer):
    data = serializers.JSONField()
    query_params = serializers.DictField(child=serializers.CharField())
    path = serializers.CharField(source="path_info")
    method = serializers.CharField()
    content_type = serializers.CharField()
    content_params = serializers.DictField(child=serializers.CharField())
    cookies = serializers.DictField(source="COOKIES", child=serializers.CharField())
    meta = serializers.DictField(source="META", child=serializers.CharField())
    headers = serializers.SerializerMethodField()
    resolver_match = ResolverMatchSerializer(required=True)
    auth = APIGatewayAuthenticationDetailsSerializer(required=False)

    def get_headers(self, request):
        "Redact any sensitive headers from the request."
        return {
            k: v if k.lower() not in SENSITIVE_HEADERS else "{REDACTED}"
            for k, v in request.headers.items()
        }


class InputDataSerializer(serializers.Serializer):
    request = RequestSerializer()
    object = serializers.JSONField()


class OPAPolicyError(RuntimeError):
    pass


def _post_url(url: str, data: bytes):
    resp = requests.post(url, data=data)
    resp.raise_for_status()
    return resp.content


class AllowedByOPAPolicy(BasePermission):
    _client_session: requests.Session

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._post_url = import_string(
            getattr(settings, "OPA_POST_URL_CALLABLE", "api.authorization._post_url")
        )
        self._package_path = "/".join(
            urllib.parse.quote(part)
            for part in getattr(settings, "OPA_PACKAGE_NAME", "authz").split(".")
        )
        self._rule_name = getattr(settings, "OPA_RULE_NAME", "allow")
        self._data_url = urllib.parse.urljoin(
            settings.OPA_API_URL,
            f"v1/data/{self._package_path}/{self._rule_name}",
        )

    def has_permission(self, request, view):
        input_data = self._construct_input(request, view)
        decision = self._get_decision(input_data)
        allow = decision.get("result", False)
        LOG.info(
            "Evaluated OPA policy",
            allow=allow,
            package_path=self._package_path,
            rule_name=self._rule_name,
            decision_id=decision.get("decision_id"),
        )
        return allow

    def has_object_permission(self, request, view, obj):
        return True

    def _get_decision(self, input_data):
        json_data = JSONRenderer().render({"input": input_data})
        try:
            return json.loads(self._post_url(self._data_url, json_data))
        except Exception:
            LOG.exception(
                "Could not get OPA policy decision",
                data_url=self._data_url,
            )
            raise

    def _construct_input(self, request, view, obj=None, *, is_for_object=False):
        if obj is not None:
            obj_data = view.get_serializer(obj).data
        else:
            obj_data = None
        return InputDataSerializer(
            {"request": request, "object": obj_data, "is_for_object": is_for_object}
        ).data


class AllowedByOPAPolicyWithObjectInput(AllowedByOPAPolicy):
    def has_object_permission(self, request, view, obj):
        input_data = self._construct_input(request, view, obj)
        decision = self._get_decision(input_data)
        allow = decision.get("result", False)
        LOG.info(
            "Evaulated OPA policy for request and specific object",
            allow=allow,
            package_path=self._package_path,
            rule_name=self._rule_name,
            decision_id=decision.get("decision_id"),
        )
        return allow
