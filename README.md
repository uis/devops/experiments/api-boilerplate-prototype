# Django Boilerplate Boilerplate Webapp

## Change from upstream

- No UI or UI-related stuff since APIs don't have UIs.
- Move to poetry for packaging. Provides automatic venv management.
- Use Poe the poet to can commonly used tasks.
- Add in API Gateway emulator.
- Use OPA for authorisation policies.
- Re-work docker compose setup:
  - Use healthchecks to ensure services start when others are ready removing
    wait-for-it hacks. Use profiles to select between development, production,
  - tox etc instead of wrapper scripts.
- Enable profiling in development by adding `prof` query parameter.
- Disable admin by default unless `ENABLE_ADMIN` is true. (Production app
  doesn't have admin enabled but a separate app with the admin enabled may be
  deployed as an internal service.)
- Future-proof ourselves by moving to Django's ASGI support.
- Move non-API related routes under `/-/`.
- API schema is available at `/-/schema.{yaml,json}`.
- In development, have swagger UI at `/-/swagger-ui/` and redoc at `/-/redoc/`.

**THIS README NEEDS COMPLETING**

TODO: Note that `PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring poetry
install` can be needed on Linux machines

To generate tree of files likely to be in boilerplate:
`tree --gitignore -I vendor/ -I simpleidentity -I __pycache__ -I auth.py | less`

## Getting started with local development

Clone this repository and `cd` to its root directory.

Install [docker engine and compose](https://docs.docker.com/engine/install/) if
you have not already done so. **YOU MUST USE DOCKER COMPOSE VERSION 2 OR
GREATER.**

Install and enable [pipx](https://pypa.github.io/pipx/) if you have not already
done so.

### Running the application

Install poetry via:

```console
pipx install poetry
pipx inject poetry poethepoet[poetry_plugin]
```

Install application dependencies via:

```console
poetry install
```

Start the application via:

```console
poetry poe up
```

Press Ctrl-C to stop the application.

> **TIP:** You can see all configured `poe` tasks by just running the `poe`
> command with no options.

You now have the following endpoints:

- [http://localhost:8000](http://localhost:8000) - the application itself hosted
  behind the API Gateway emulator proxy.
- [http://localhost:8001](http://localhost:8001) - a UI to change the API
  Gateway emulator settings.

Some lesser used endpoints:

- [http://localhost:9000](http://localhost:9000) - the application without the
  API Gateway proxy.
- [http://localhost:9001](http://localhost:9001) - the Open Policy Agent (OPA)
  instance providing local authorization decisions.

A local PostgreSQL database is available at `localhost` on port 9876. The
default database, username and password can be found in the [docker compose
configuration](docker-compose.yml).

### Local development

Install dependencies via:

```console
poetry install
```

Install git pre-commit hooks via:

```console
poetry run pre-commit install
```

## VSCode debugger support

There is basic support for running the server and attaching VSCode's debugger:

1. Open this repository in VSCode.
2. Select Run > Start Debugging from the menu bar to start the application in
   developer mode and attach to the web server process.
3. Add breakpoints, etc and debug away. Container logs can be viewed using the
   VSCode docker extension.
4. Select Run > Stop Debugging from the menu bar to stop all containers.

## Cheat sheet

Some commonly used commands are shown below.

### Running the application

To spin up a local database, run migrations and start the application in _debug
mode_:

```console
poetry poe up
```

To spin up a local database, run migrations and start the application _from the
production container_:

```console
poetry poe up:production
```

Press Ctrl-C to stop the application.

To make sure that the application is stopped _but to keep the local database's
contents_:

```console
poetry poe down
```

To make sure that the application is stopped, delete any local database contents
and remove any containers which may still be running:

```console
poetry poe down:hard
```

To build or rebuild all container images used by the application:

```console
poetry poe compose:build
```

To build or rebuild all container images used by the application and pull any
new base images:

```console
poetry poe compose:build --pull
```

To pull any images used by the docker compose configuration:

```console
poetry poe compose:pull
```

### Common development tasks

To run pre-commit checks in order to fix up code style and layout to match our
common convention:

```console
poetry poe fix
```

To run the Python test suite via tox:

```console
poetry poe tox
```

To run the OPA test suite:

```console
poetry poe opa:test
```

To run Django management commands:

```console
poetry poe manage {command}
```

To create new migrations after a change of model:

```console
poetry poe manage makemigrations {app} -n "{human_friendly_name_of_migration}"
```

**The resulting migrations will be owned by "root" on Linux systems.** You may
want to change ownership after creation by running `sudo chown -R "$USER" .` in
the repository root.

**This shell will run _inside_ the container and so files you create will be
owned by "root".**

To run a database migration:

```console
poetry poe manage migrate
```

To run the Python test suite via tox _using the locally installed version of
Python_:

```console
poetry poe tox:local
```

To run the Python test suite directly via `pytest`:

```console
poetry run pytest .
```

> Some IDEs, such as VSCode, will run `pytest` directly like this and so it's
> important to check that doing so still works.

Both `tox` and `tox:local` can take additional arguments which are passed to
`tox`. Use this to, for example, only run a subset of tests:

```console
poetry poe tox -e py3 -- path/to/test_module.py
```

To open an interactive Python shell which can use the Django database models:

```console
poetry poe manage shell
```

### Dependencies

> **IMPORTANT:** if you add a new dependency to the application as described
> below you will need to run `docker compose build` or add `--build` to the
> `docker compose run` and/or `docker compose up` command at least once for
> changes to take effect when running code inside containers.

To add a new dependency _for the application itself_:

```console
poetry add {dependency}
```

To add a new dependency _used only when the application is running locally in
development_:

```console
poetry poe add:dev {dependency}
```

To add a new dependency _used only to run the test suite_:

```console
poetry poe add:testing {dependency}
```

To remove a dependency which is no longer needed:

```console
poetry remove {dependency}
```

For example:

- Django 4.2 is required to run the application at all. It was added via
  `poetry add Django~=4.2.0`.
- The Django debug toolbar is required only when running locally in development.
  It was added via `poetry poe add:dev django-debug-toolbar`.
- The pytest library is only used when running tests. It was added via
  `poetry poe add:testing testing pytest`.

### Running commands within the virtual environment

To run a command within the automatically created virtual environment, use
`poetry run`. For example:

```console
poetry run tox  # runs the test suite
```

To launch a command line shell inside the virtual environment:

```console
poetry shell
```

To delete the automatically created Python virtual environment in order to save
disk space:

```console
poetry env remove python3
```
